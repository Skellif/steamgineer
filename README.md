# Project Steamgineer

Rogue-like mixed with tower defence, where you must Build steampunk machinery to survive waves of enemys and reach deapest caverns.

## Getting Started



### Prerequisites

Unity 2018
Any code editor

```
MS Visual Studio
```

### Installing



## Running the tests



### Break down into end to end tests



### And coding style tests



## Deployment



## Built With

* [Unity 2018](https://unity3d.com/) - Game Engine

## Contributing



## Versioning



## Authors

* **Jakub Sarno** - *Idea, Programming* - [Skellif](https://gitlab.com/Skellif)
* **Marcin Wola** - *Programming* - [mwarw](https://gitlab.com/mwarw)
* **Piotr Bartosz** - *Programming* - [The_Phantom](https://gitlab.com/The_Phantom)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired by Factorio
* Inspired by Dungeon of the Endless
